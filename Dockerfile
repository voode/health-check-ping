FROM busybox:1.31.1

COPY rootfs /

EXPOSE 8080
CMD [ "httpd", "-f", "-h", "/var/www/html", "-p", "8080", "-c", "/etc/httpd.conf" ]

HEALTHCHECK --interval=5s --timeout=1500ms --start-period=10s --retries=3 CMD ["/docker-healthcheck.sh"]
