Response with status 200 for http GET and HEAD requests

```bash
curl -I -X HEAD localhost:8080/ping
```
